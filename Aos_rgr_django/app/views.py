"""
Definition of views.
"""

from django.shortcuts import render
from django.http import HttpRequest
from django.template import RequestContext
from datetime import datetime
import json

from .models import weather



def home(request):
    """Renders the home page."""
    #assert isinstance(request, HttpRequest)

    cur_info = weather.objects.last()

    return render(
        request,
        'app/index.html',
        {
            'cur_temperature' : cur_info.temperature,
            'cur_humidity' : cur_info.humidity,
            'cur_pressure' : cur_info.pressure,
            'cur_time' : cur_info.date,
            }
    )

def graph(request):

    all_info = weather.objects.all()

    current_weather = [
        {'time': str(i.date),
        'temperature' : float(i.temperature),
        'humidity' : float(i.humidity),
        'pressure' : float(i.pressure),
        } for i in all_info
    ]

    result = json.dumps(current_weather)

    return render(
        request,
        'app/graph.html',
        {
            'json_str' : result,
            }
    )