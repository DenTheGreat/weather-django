const data = JSON.parse(json_string);
const time = data.map(count => {
	return count.time;
});
const temperature = data.map(count => {
	return count.temperature;
});
const humidity = data.map(count => {
	return count.humidity;
});
const pressure = data.map(count => {
	return count.pressure;
});

let tmprContext = document.querySelector(".tmpr-chart").getContext("2d");
let prsContext = document.querySelector(".prs-chart").getContext("2d");
let hmdtContext = document.querySelector(".hmdt-chart").getContext("2d");

let tmprChart = new Chart(tmprContext, {
	type: 'line',
	data: {
		labels: time,
		datasets: [{
			label: 'temperature',
			fill: false,
			data: temperature,
			borderColor: 'rgb(255, 255, 0)'
		}]
	},
	options: {
		scales: {
			yAxes: [{
				ticks: {
					beginAtZero: true
				}
			}]
		}
	}
});

let prsChart = new Chart(prsContext, {
	type: 'line',
	data: {
		labels: time,
		datasets: [{
			label: 'pressure',
				fill: false,
				data: pressure,
				borderColor: 'rgb(255, 12, 0)'
		}]
	},
	options: {
		scales: {
			yAxes: [{
				ticks: {
					min: 975,
					max: 1005
				}
			}]
		}
	}
});

let hmdtChart = new Chart(hmdtContext, {
	type: 'line',
	data: {
		labels: time,
		datasets: [{
			label: 'humidity',
				fill: false,
				data: humidity,
				borderColor: 'rgb(0, 0, 255)'
		}]
	},
	options: {
		scales: {
			yAxes: [{
				ticks: {
					beginAtZero: true
				}
			}]
		}
	}
});


