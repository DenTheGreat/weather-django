"""
Definition of models.
"""

from django.db import models

class weather(models.Model):    
    date = models.DateTimeField()
    temperature = models.CharField(max_length=50)
    humidity = models.CharField(max_length=50)
    pressure = models.CharField(max_length=50)

    def __str__(self):
        return str(self.date)
